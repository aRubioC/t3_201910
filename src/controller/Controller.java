package controller;

import java.util.Scanner;

import model.data_structures.DaylyStatistic;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.MovingViolations;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.logic.MovingViolationsManager;
import model.vo.VODaylyStatistic;
import model.vo.VOMovingViolations;
import view.MovingViolationsManagerView;

public class Controller {
 
	private MovingViolationsManagerView view;
	
	private final static String january = "Moving_Violations_Issued_in_January_2018_ordered.csv";
	
	private final static String february = "Moving_Violations_Issued_in_February_2018_ordered.csv";
	
	/**
	 * Cola donde se van a cargar los datos de los archivos
	 */
	private Queue<VOMovingViolations> movingViolationsQueue;
	
	/**
	 * Pila donde se van a cargar los datos de los archivos
	 */
	private Stack<VOMovingViolations> movingViolationsStack;
	
	private MovingViolationsManager manager = new MovingViolationsManager();

	public Controller() {
		view = new MovingViolationsManagerView();
		
		
		//TODO, inicializar la pila y la cola
		movingViolationsQueue = null;
		movingViolationsStack = null;
	}
	
	public void run() {
		Scanner sc = new Scanner(System.in);
		boolean fin = false;
		
		while(!fin)
		{
			view.printMenu();
			
			int option = sc.nextInt();
			
			switch(option)
			{
				case 1:
					this.loadMovingViolations();
					break;
					
				case 2:
					Queue<DaylyStatistic> dailyStatistics = this.getDailyStatistics();
					view.printDailyStatistics(dailyStatistics);
					break;
					
				case 3:
					view.printMensage("Ingrese el número de infracciones a buscar");
					int n = sc.nextInt();

					Stack<MovingViolations> violations = this.nLastAccidents(n);
					view.printMovingViolations(violations);
					break;
											
				case 4:	
					fin=true;
					sc.close();
					break;
			}
		}
	}

	

	public void loadMovingViolations() 
	{
		manager.loadMovingViolations(january);
		
		manager.loadMovingViolations(february);
	}
	
	public Queue<DaylyStatistic> getDailyStatistics () {
		// TODO
		return manager.getDalyStatistics();
	}
	
	public Stack <MovingViolations> nLastAccidents(int n) {
		// TODO
		return manager.nLastAccidents(n);
	}
}
