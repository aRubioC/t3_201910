package model.data_structures;

public class DaylyStatistic {

	private String ticketIssueDate;
	
	private int numeroAccidentes;
	
	private int numeroInfracciones;
	
	private int totalFINEAMT;
	
	public DaylyStatistic(String pTicket, int pNumeroAccidentes, int pNumeroInfracciones, int pTotalFINEAMT)
	{
		ticketIssueDate = pTicket;
		numeroAccidentes = pNumeroAccidentes;
		numeroInfracciones = pNumeroInfracciones;
		totalFINEAMT = pTotalFINEAMT;
	}
	
	public String getTicketIssueDate()
	{
		return ticketIssueDate;
	}
	
	public int getNumeroAccidentes()
	{
		return numeroAccidentes;
	}
	
	public int getNumeroInfracciones()
	{
		return numeroInfracciones;
	}
	
	public int getTotalFINEAMT()
	{
		return totalFINEAMT;
	}

}
