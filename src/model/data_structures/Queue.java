package model.data_structures;

import static org.junit.Assert.assertNotNull;

import java.util.Iterator;

public class Queue<T> implements IQueue<T> 
{

	private Node<T> start;
	
	private Node<T> end;
	
	private int size;
	
	public Queue() 
	{
		// TODO Auto-generated constructor stub
		
		start = null;
		
		end = null;
		
		size = 0;
	}
	
	@Override
	public Iterator<T> iterator()
	{
		return new ListIterator();
	}

	@Override
	public boolean isEmpty() 
	{
		boolean rta = false;
		
		if(size == 0)
		{
			rta = true;
		}
		
		return rta;
	}

	@Override
	public int size() 
	{
		Node<T> stat = start;
		
		size = 0;
		
		if(start != null)
		{
			while(stat != null)
			{
				size ++;
				stat = stat.getNext();
			}
		}
		
		return size;
	}

	@Override
	public void enqueue(T t) 
	{
		// TODO Auto-generated method stub
		Node<T> ness = new Node<T>(t);
		if(start == null)
		{
			start = ness;
		}
		else
		{
			if(end == null)
			{
				calculateSize();
			}			
			end.setNext(ness);
			end = ness;
		}
		size++;
	}

	@Override
	public T dequeue() 
	{
		// TODO Auto-generated method stub
		
		Node<T> stat = start;
		
		if(start == null)
		{
			return null;
		}
		else if(size > 1)
		{
			start = start.getNext();
			start.setPrevious(null);
			stat.setNext(null);
			size--;
			return stat.getValue();
		}
		else
		{
			start = null;
			return stat.getValue();
		}
	}
	
	public void calculateSize()
	{
		size = 0;
		Node<T> current = start;
		while(current != null)
		{
			size ++;
			end = current;
			current = current.getNext();
		}
	}
	
	private class ListIterator implements Iterator<T> 
	 {
	        private Node<T> current = start;
	        public boolean hasNext()  
	        { 
	        	return current != null;                     
	        }
	        public void remove()      
	        { 
	        	throw new UnsupportedOperationException();  
	        }

	        public T next() 
	        {
	            T item = current.getValue();
	            current = current.getNext(); 
	            return item;
	        }
	    }

}
