package model.data_structures;

import java.util.Iterator;

public class Stack<T> implements IStack<T> 
{

	private Node<T> start;

	private int size;

	public Stack() 
	{
		// TODO Auto-generated constructor stub

		start = null;

		size = 0;
	}

	@Override
	public boolean isEmpty() 
	{
		return start==null;
	}

	@Override
	public int size() 
	{
		return size;
	}

	@Override
	public void push(T t) 
	{
		// TODO Auto-generated method stub
		Node<T> oldstart = start;
		start = new Node<T>(t);
		start.setValue(t);
		start.setNext(oldstart);
		size++;
	
	}

	@Override
	public T pop() 
	{
		T e;
		// TODO Auto-generated method stub
		if (start == null)
		{
			return null;
		}
		else
		{	
			e= start.getValue();        
			start = start.getNext();            
			size--;
		}
		return e; }

		@Override
		public Iterator<T> iterator()
		{
			return new Iterator<T> (){

				Node<T> current = null;

				public boolean hasNext()  
				{ 
					if(size == 0)
						return false;
					if(current == null)
						return true;
					return current.getNext() != null;
				}


				public T next() 
				{
					if (current == null)
						current = start;
					else
						current.getNext();
					return current.getValue();
				}
			};
		}
	}




