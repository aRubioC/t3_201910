package model.logic;

import model.data_structures.DaylyStatistic;
import model.data_structures.MovingViolations;
import model.data_structures.Queue;
import model.data_structures.Stack;
import sun.net.www.content.image.png;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
public class MovingViolationsManager
{

	private Stack<MovingViolations> stack;

	private Queue<MovingViolations> queue;

	public void loadMovingViolations(String movingViolationsFile)
	{
		Path path = FileSystems.getDefault().getPath("data", movingViolationsFile);
		Reader reader;
		try 
		{
			reader = Files.newBufferedReader(path);

			CSVParser parser = new CSVParserBuilder().withSeparator(',').withIgnoreQuotations(true).build();

			CSVReader csvReader = new CSVReaderBuilder(reader).withSkipLines(1).withCSVParser(parser).build();

			if(stack == null && queue == null)
			{
				stack = new Stack<MovingViolations>();
				queue = new Queue<MovingViolations>();
			}

			ArrayList list = new ArrayList();
			String[] line;
			while ((line = csvReader.readNext()) != null) 
			{
				if(line[0].equals("") || line[2].equals("") || line[13].equals("") || line[9].equals("") || line[12].equals("") || line[14].equals("") || line[8].equals("") || line[15].equals(""))
				{
					list.add(line);
				}
				else
				{
					MovingViolations movet = new MovingViolations(Integer.parseInt(line[0]), line[2], line[13], Integer.parseInt(line[9]), line[12], line[14], Integer.parseInt(line[8]), line[15]);
					stack.push(movet);
					queue.enqueue(movet);
				}

			}	
			reader.close();
			csvReader.close();

			System.out.println("Se cargaron exitosamente " + stack.size() + " registros de pila");
			System.out.println("Se cargaron exitosamente " + queue.size() + " registros de cola");
			System.out.println("No se pudieron cargar " + list.size() + " registros");




		}
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	


	}

	public Queue<DaylyStatistic> getDalyStatistics()
	{
		Queue<DaylyStatistic> ans = new Queue<DaylyStatistic>();
		MovingViolations element = queue.dequeue();
		int tamano = queue.size();

		String pTicket = element.getTicketIssueDate();
		String fechaActual = pTicket;
		int pNumeroAccidentes = 0;
		int pNumeroInfracciones = 0;
		int pTotalFINEAMT = 0;

		for(int i = 0; i < tamano + 1; i++)		

		{
			fechaActual = element.getTicketIssueDate();
			String[] fecha = fechaActual.split("T");
			String[] act = pTicket.split("T");

			if(fecha[0].equals(act[0]) && i < tamano)
			{
				if (element.getAccidentIndicator().equals("Yes") )
				{
					pNumeroAccidentes++;
				}
				pNumeroInfracciones++;
				pTotalFINEAMT = pTotalFINEAMT + element.getFineAmt();
				element = queue.dequeue();	
				if (element == null)
				{		
					DaylyStatistic ad = new DaylyStatistic(act[0], pNumeroAccidentes, pNumeroInfracciones, pTotalFINEAMT);
					ans.enqueue(ad);

				}
			}
			else
			{
				DaylyStatistic ad = new DaylyStatistic(act[0], pNumeroAccidentes, pNumeroInfracciones, pTotalFINEAMT);
				ans.enqueue(ad);

				pNumeroInfracciones = 0;
				pTotalFINEAMT = 0;
				pNumeroAccidentes = 0;
				pTicket = element.getTicketIssueDate();

			}
		}
		return ans;
	}

	public Stack<MovingViolations> nLastAccidents(int n)
	{
		Stack<MovingViolations> ans = new Stack<MovingViolations>();
		int tamano = stack.size();
		int k = 0;

		for(int i = 0; i < tamano && k < n; i++)
		{
			MovingViolations anss = stack.pop();

			if(anss.getAccidentIndicator().equals("Yes"))
			{
				ans.push(anss);
				k++;
			}
		}

		return ans;
	}
}
