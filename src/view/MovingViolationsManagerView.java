package view;

import model.data_structures.DaylyStatistic;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.MovingViolations;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.VODaylyStatistic;
import model.vo.VOMovingViolations;

public class MovingViolationsManagerView 
{
	public MovingViolationsManagerView() {
		
	}
	
	public void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 3----------------------");
		System.out.println("1. Cree una nueva coleccion de infracciones en movimiento");
		System.out.println("2. Dar estadisticas diarias de las infracciones");
		System.out.println("3. Dar ultimos n infracciones que terminaron en accidente");
		System.out.println("4. Salir");
		System.out.println("Digite el n�mero de opci�n para ejecutar la tarea, luego presione enter: (Ej., 1):");
		
	}
	
	public void printDailyStatistics(Queue<DaylyStatistic> dailyStatistics) {
		
		for (DaylyStatistic dayStatistic : dailyStatistics) 
		{
			//TODO
			System.out.println( dayStatistic.getTicketIssueDate() + "- accidentes:	" + dayStatistic.getNumeroAccidentes() + ",	infracciones:	" + dayStatistic.getNumeroInfracciones() + ",	multas totales:	$" + dayStatistic.getTotalFINEAMT() );;
		}
		System.out.println("Se encontraron "+ dailyStatistics.size() + " elementos");
	}
	
	public void printMovingViolations(Stack<MovingViolations> violations) {
		System.out.println("Se encontraron "+ violations.size() + " elementos");
		for (MovingViolations violation : violations) 
		{
			System.out.println( "ID: " + violation.getObjectId() + " Fecha: " 
								+ violation.getTicketIssueDate() + " En: " 
								+ violation.getLocation()+ " Descripci�n: " 
								+ violation.getViolationDescription());
		}
	}
	
	public void printMensage(String mensaje) {
		System.out.println(mensaje);
	}
}
