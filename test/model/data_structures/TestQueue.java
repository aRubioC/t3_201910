package model.data_structures;

import java.util.ArrayList;

import junit.framework.TestCase;
import model.data_structures.Node;
import model.data_structures.Queue;;

public class TestQueue<T> extends TestCase
{

	private Queue<T> queue;
	
	private int numeroNodos;
	
	private void setupEscenario1()
	{
		queue = new Queue<T>();
		numeroNodos = 0;
	}
	
	private void setupEscenario2()
	{
		queue = new Queue<T>();
		numeroNodos = 10;
		for(Integer i = 0; i < numeroNodos; i++)
		{
			queue.enqueue((T) i);
		}
	}
	
	public void testQueue()
	{
		setupEscenario1( );
        numeroNodos = 11;
        ArrayList<Node<T>> linked2 = new ArrayList( );
        Node<T> lol = null;
        for( Integer cont = 0; cont < numeroNodos; cont++ )
        {    
            linked2.add(new Node<T>((T)cont) );
        }
        
        for(int i = 0; i < linked2.size(); i++)
        {
    		Node<T> nodes = (Node<T>) linked2.get(i);
            queue.enqueue(nodes.getValue());
        }
        ArrayList<T> lesNodes = new ArrayList<T>();
        // Agrega los pacientes y verifica que se hayan agregado correctamente
        for(Integer cont = 0; cont < numeroNodos; cont++ )
        {
        	Node<T> nodez = (Node<T>) linked2.get(cont);
        
            
            lesNodes.add(queue.dequeue());
            lol = new Node<T>(lesNodes.get(cont));

            // Verifica que la informaci�n sea correcta
            assertEquals( "La adici�n no se realiz� correctamente", nodez.getValue(), lol.getValue() );

            // Verifica que el n�mero de pacientes en la lista sea el correcto
            assertEquals( "El n�mero de nodos no es el correcto", (cont + 1), lesNodes.size());
        }
	}
	
	public void testDequeue()
	{
		setupEscenario2();
		Node<T> nodes = new Node<T>(queue.dequeue());
		
		assertEquals("El nodo no debio encontrarse", 0, nodes.getValue());
		assertEquals("El numero de nodos no es el correcto",(numeroNodos - 1), queue.size());
	}
	

}
