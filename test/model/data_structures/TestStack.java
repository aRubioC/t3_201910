package model.data_structures;

import java.util.ArrayList;

import junit.framework.TestCase;

public class TestStack<T> extends TestCase
{
	private Stack<T> stack;
	
	private int numeroNodos;
	
	private void setupEscenario1()
	{
		stack = new Stack<T>();
		numeroNodos = 0;
	}
	
	private void setupEscenario2()
	{
		stack = new Stack<T>();
		numeroNodos = 10;
		for(Integer i = 0; i < numeroNodos; i++)
		{
			stack.push((T) i) ;
		}
	}
	
	public void testQueue()
	{
		setupEscenario1( );
        numeroNodos = 11;
        ArrayList<Node<T>> linked2 = new ArrayList( );
        Node<T> lol = null;
        for( Integer cont = 0; cont < numeroNodos; cont++ )
        {    
            linked2.add(new Node<T>((T)cont) );
        }
        
        for(int i = 0; i < linked2.size(); i++)
        {
    		Node<T> nodes = (Node<T>) linked2.get(i);
            stack.push(nodes.getValue());
        }
        ArrayList<T> lesNodes = new ArrayList<T>();
        int conte = (numeroNodos - 1);
        // Agrega los pacientes y verifica que se hayan agregado correctamente
        for(Integer cont = 0; cont < numeroNodos; cont++ )
        {
        	Node<T> nodez = (Node<T>) linked2.get(conte);
        
            
            lesNodes.add(stack.pop());
            lol = new Node<T>(lesNodes.get(cont));

            // Verifica que la informaci�n sea correcta
            assertEquals( "La adici�n no se realiz� correctamente", nodez.getValue(), lol.getValue() );

            // Verifica que el n�mero de pacientes en la lista sea el correcto
            assertEquals( "El n�mero de nodos no es el correcto", (cont + 1), lesNodes.size());
            conte--;
        }
	}
	
	public void testDequeue()
	{
		setupEscenario2();
		Node<T> nodes = new Node<T>(stack.pop());
		
		assertEquals("El nodo no debio encontrarse", 9, nodes.getValue());
		assertEquals("El numero de nodos no es el correcto",(numeroNodos - 1), stack.size());
	}
}
